package zti.todo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import zti.todo.model.Todo;
import zti.todo.repository.TodoRepository;

@Service
public class TodoService {
	
	private final TodoRepository todoRepository;
	
	@Autowired
	TodoService(@Qualifier("jpa") TodoRepository todoRepository){
		this.todoRepository = todoRepository;
	}
	
	// get all todos
	public Iterable<Todo> getAllTodos(){
		return todoRepository.findAll();
	}
	
	// get by id
	public Optional<Todo> getTodoById(Integer id) {
		return todoRepository.findById(id);
	}
	
	// add new todo
	public int addTodo(Todo todo) {
		todoRepository.save(todo);
		return 1;
	}
	
	// delete todo
	public int deleteTodo(Integer id) {
		todoRepository.deleteById(id);
		return 1;
	}
	
	// update todo // if not exist save, if exist update
	public void updateTodo(Integer id, Todo todo) {
		Optional<Todo> todoFromDb = todoRepository.findById(id);
	    if(todoFromDb.isEmpty()) {
	    	todoRepository.save(todo);
	    } else {
	    	Todo newTodo = new Todo();
	    	newTodo.setId(id);
	    	newTodo.setTitle(todo.getTitle());
	    	todoRepository.save(newTodo);
	    }
	}

}
