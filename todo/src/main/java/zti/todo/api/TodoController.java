package zti.todo.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import zti.todo.model.Todo;
import zti.todo.service.TodoService;

@RequestMapping("api/v1/todos")
@RestController
@CrossOrigin("*")
public class TodoController {
	
	private final TodoService todoservice;
	
	@Autowired
	TodoController(TodoService todoservice){
		this.todoservice = todoservice;
	}

	@GetMapping
	public @ResponseBody Iterable<Todo> getTodos(){
		Iterable<Todo> todos = todoservice.getAllTodos();
		System.out.println(todos.toString());
		return todos;
	}
	
	@GetMapping(path = "{id}")
	public @ResponseBody Todo getTodoById(@PathVariable("id") Integer id){
		return todoservice.getTodoById(id).orElse(null);
	}
	
	@PostMapping
	public void addTodo(@RequestBody Todo newTodo) {
		todoservice.addTodo(newTodo);
	}
	
	@PutMapping(path = "{id}")
	public void updateTodo(@PathVariable("id") Integer id, @RequestBody Todo todo) {
		todoservice.updateTodo(id, todo);
	}
	
	@DeleteMapping(path = "{id}")
	public void delTodo(@PathVariable("id") Integer id){
		todoservice.deleteTodo(id);
	}
	
}
