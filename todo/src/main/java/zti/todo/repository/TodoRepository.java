package zti.todo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import zti.todo.model.Todo;

@Repository("jpa")
public interface TodoRepository extends CrudRepository<Todo, Integer> {}
