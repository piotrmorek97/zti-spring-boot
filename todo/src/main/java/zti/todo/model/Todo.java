package zti.todo.model;

import javax.persistence.*;

@Entity
@Table(name = "todos")
public class Todo{

	private String title;
	private Integer id;
	
	public Todo() {}

	@Id
	// value will be automatically generated for that field
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
